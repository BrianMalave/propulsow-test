import { Info, Navbar, Hero, Items } from './components/indexComponents'
import './App.css'

const App = () => (
  <div>
    <Info />
    <Navbar />
    <Hero />
    <Items />
  </div>
);

export default App
