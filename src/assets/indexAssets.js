import logo from './logo.png';
import lupa from  './lupa.svg';
import bag from './bag.svg';
import slider from './slider.png';
import producto from './producto.png';

export {
  logo,
  lupa,
  bag,
  slider,
  producto
};