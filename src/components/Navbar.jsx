import { logo, lupa, bag } from "../assets/indexAssets";

const Navbar = () => {
  return (
    <div className="nav-container">
      <nav className="navbar">
        <div>
          <img className="logo-navbar" src={logo} alt="logo"/>
        </div>
        <div className="nav-img-links">
          <div className="nav-link">
            <ul>
              <li><a href="#">Nosotros</a></li>
              <li><a href="#">Galeria</a></li>
              <li><a href="#">Servicios</a></li>
              <li><a href="#">Productos</a></li>
              <li><a href="#">Contacto</a></li>
              <li><a href="#">Icono Lupa</a></li>
              <li><a href="#">Icono Minicarro</a></li>
            </ul>
          </div>
          <div className="icons-navbar">
            <img src={lupa} alt="buscador" />
            <img src={bag} alt="bolsa" />
          </div>
        </div>
      </nav>
    </div>
  );
}
export default Navbar