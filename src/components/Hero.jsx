import { slider } from "../assets/indexAssets";

const Hero = () => {
  return (
    <div className="hero-container">
      <img src={slider} alt="imagen principal" />
    </div>
  );
}
export default Hero